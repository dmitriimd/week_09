package ru.edu.task4.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class RealService implements SomeInterface {
    @Override
    public String getName() {
        return "RealService";
    }
}
